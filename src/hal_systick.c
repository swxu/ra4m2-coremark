#include <stdio.h>
#include "hal_data.h"
#include "hal_systick.h" // #define TICKS_PER_SECONDS 1000

volatile uint32_t g_tick_count = 0;

void hal_systick_init()
{
    SysTick_Config(SystemCoreClock / TICKS_PER_SECOND);
	printf("SystemCoreClock=%d\n", SystemCoreClock);
}

void SysTick_Handler(void)
{
    g_tick_count += 1;
}

uint32_t hal_systick_get()
{
    return g_tick_count;
}