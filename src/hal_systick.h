#ifndef __HAL_SYSTICK_H__
#define __HAL_SYSTICK_H__

#include <stdint.h>

#define TICKS_PER_SECOND 1000

void hal_systick_init();

uint32_t hal_systick_get();

#endif