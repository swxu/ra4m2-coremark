#include <stdio.h>
#include <stdbool.h>

#include "r_sci_uart.h"
#include "hal_data.h"

volatile bool uart_tx_done = false;

void hal_uart9_init()
{
    R_SCI_UART_Open(&g_uart9_ctrl, &g_uart9_cfg);
}

int fputc(int ch, FILE* f)
{
    (void) f;

    uart_tx_done = false;

    R_SCI_UART_Write(&g_uart9_ctrl, (uint8_t *)&ch, 1);

    while (uart_tx_done == false);

    return ch;
}

void debug_uart9_callback(uart_callback_args_t* p_args)
{
   switch (p_args->event)
   {
      case UART_EVENT_RX_CHAR:
            break;
      case UART_EVENT_TX_COMPLETE:
            uart_tx_done = true;
            break;
      default:
            break;
   }
}